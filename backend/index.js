const express = require("express");
const mongoose = require("mongoose");
const Board = require("./models/boards.model");
const cors = require("cors");

const trelloRoutes = require("./routes/trello.routes");

const app = express();

const PORT = 3000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/boards", trelloRoutes);

app.get("/", (req, res) => {
  res.send("Hello from Trello");
});

mongoose
  .connect(
    "mongodb+srv://abhishekroshan11:abhi1234@backenddb.wa5hhfn.mongodb.net/?retryWrites=true&w=majority&appName=backendDb"
  )
  .then(() => {
    console.log("Connected to database");
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch(() => {
    console.log("Connection Failed");
  });
