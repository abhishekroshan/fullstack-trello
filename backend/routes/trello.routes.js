const express = require("express");
const router = express.Router();

const {
  getBoards,
  createBoard,
  deleteBoard,
} = require("../controllers/trello.controllers");

const {
  getLists,
  createList,
  deleteList,
} = require("../controllers/lists.controllers");
const {
  getCards,
  createCard,
  deleteCard,
} = require("../controllers/cards.controllers");

const {
  getChecklist,
  createChecklist,
  deleteChecklist,
} = require("../controllers/checklist.controllers");

// Board routes
router.get("/", getBoards);
router.post("/", createBoard);
router.delete("/:id", deleteBoard);

// Lists routes
router.get("/:boardId/lists", getLists);
router.post("/:boardId/lists", createList);
router.delete("/lists/:id", deleteList);

// Cards Route
router.get("/:listId/cards", getCards);
router.post("/:listId/cards", createCard);
router.delete("/cards/:id", deleteCard);

// Checklist Route
router.get("/:cardId/checklist", getChecklist);
router.post("/:cardId/checklist", createChecklist);
router.delete("/checklist/:id", deleteChecklist);

module.exports = router;
