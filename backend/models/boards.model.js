const mongoose = require("mongoose");

const BoardsSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Enter Board Name"],
    },
  },
  {
    timestamps: true,
  }
);

const Board = mongoose.model("Board", BoardsSchema);

module.exports = Board;
