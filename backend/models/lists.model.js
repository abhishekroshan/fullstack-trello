const mongoose = require("mongoose");

const ListsSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },

    boardId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Board",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const List = mongoose.model("List", ListsSchema);

module.exports = List;
