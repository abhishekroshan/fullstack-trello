const mongoose = require("mongoose");

const CardsSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },

    listId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "List",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const Card = mongoose.model("Card", CardsSchema);

module.exports = Card;
