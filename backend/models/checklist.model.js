const mongoose = require("mongoose");

const ChecklistsSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    cardId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Card",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const ChecklistModel = mongoose.model("Checklist", ChecklistsSchema);

module.exports = ChecklistModel;
