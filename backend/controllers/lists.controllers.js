const List = require("../models/lists.model");

const getLists = async (req, res) => {
  try {
    const lists = await List.find({ boardId: req.params.boardId });
    res.status(200).json(lists);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const createList = async (req, res) => {
  try {
    const list = await List.create({
      title: req.body.title,
      boardId: req.params.boardId,
    });
    res.status(201).json(list);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteList = async (req, res) => {
  try {
    const { id } = req.params;

    const list = await List.findByIdAndDelete(id);

    if (!list) {
      return res.status(404).json({ message: "List Not Found" });
    }

    res.status(200).json({ message: "List Deleted Successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  getLists,
  createList,
  deleteList,
};
