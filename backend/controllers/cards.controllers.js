const Card = require("../models/cards.model");

const getCards = async (req, res) => {
  try {
    const cards = await Card.find({ listId: req.params.listId });
    res.status(200).json(cards);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const createCard = async (req, res) => {
  try {
    const card = await Card.create({
      title: req.body.title,
      listId: req.params.listId,
    });
    res.status(200).json(card);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteCard = async (req, res) => {
  try {
    const { id } = req.params;

    const card = await Card.findByIdAndDelete(id);

    if (!card) {
      return res.status(404).json({ message: "Card Not Found" });
    }

    res.status(200).json({ message: "Card Deleted Successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  getCards,
  createCard,
  deleteCard,
};
