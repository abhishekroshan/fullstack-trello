const Board = require("../models/boards.model");

const getBoards = async (req, res) => {
  try {
    const boards = await Board.find({});
    res.status(200).json(boards);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const createBoard = async (req, res) => {
  try {
    const board = await Board.create(req.body);
    res.status(200).json(board);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteBoard = async (req, res) => {
  try {
    const { id } = req.params;

    const board = await Board.findByIdAndDelete(id);

    if (!board) {
      return res.status(404).json({ message: "Board Not Found" });
    }

    res.status(200).json({ message: "Board Deleted SuccessFully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// const getLists = async (req, res) => {
//   try {
//     const boards = await Board.find({});
//     res.status(200).json(boards);
//   } catch (error) {
//     res.status(500).json({ message: error.message });
//   }
// };

module.exports = {
  getBoards,
  createBoard,
  deleteBoard,
};
