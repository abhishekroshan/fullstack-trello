const Checklist = require("../models/checklist.model");

const getChecklist = async (req, res) => {
  try {
    const checklist = await Checklist.find({ cardId: req.params.cardId });
    res.status(200).json(checklist);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const createChecklist = async (req, res) => {
  try {
    const checklist = await Checklist.create({
      title: req.body.title,
      cardId: req.params.cardId,
    });

    res.status(200).json(checklist);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteChecklist = async (req, res) => {
  try {
    const { id } = req.params;

    const checklist = await Checklist.findByIdAndDelete(id);

    if (!checklist) {
      return res.status(404).json({ message: "Checklist Not Found" });
    }

    res.status(200).json({ message: "Checklist Deleted Successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  getChecklist,
  createChecklist,
  deleteChecklist,
};
