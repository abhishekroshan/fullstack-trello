import { useState, useEffect } from "react";
import Boards from "./components/Boards";

function App() {
  // const [boards, setBoards] = useState([]);
  // const [errors, setErrors] = useState(false);
  // const [loading, setLoading] = useState(true);

  // const addBoard = async (newBoardData) => {
  //   try {
  //     const res = await addBoard(newBoardData);
  //     console.log(res);
  //     setBoards((prevBoards) => [...prevBoards, res.data]);
  //   } catch (error) {}
  // };

  // useEffect(() => {
  //   const fetchBoard = async () => {
  //     try {
  //       const data = await fetchBoardsData();
  //       // console.log(data.data);
  //       setBoards(data.data);
  //       setLoading(false);
  //     } catch (error) {
  //       console.log(error);
  //       setErrors(true);
  //     }
  //   };

  //   fetchBoard();
  // }, []);

  // console.log("boards", boards);

  return (
    <>
      <Boards />
    </>
  );
}

export default App;
