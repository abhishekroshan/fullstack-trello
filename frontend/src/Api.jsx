import axios from "axios";

export const fetchBoardsData = async () => {
  try {
    const res = await axios.get("http://localhost:3000/boards");
    return res;
  } catch (error) {
    throw error;
  }
};

export const addNewBoard = async (newBoardData) => {
  try {
    const res = await axios.post("http://localhost:3000/boards", newBoardData);
    return res;
  } catch (error) {
    throw error;
  }
};

export const deleteBoard = async (id) => {
  try {
    const res = await axios.delete(`http://localhost:3000/boards/${id}`);
    return res;
  } catch (error) {
    throw error;
  }
};

//==========================================================================

export const fetchListsData = async (boardsId) => {
  try {
    // const res = await axios.get(`https://api.trello.com/1/boards/${id}/lists`);
    const res = await axios.get(
      `http://localhost:3000/boards/${boardsId}/lists`
    );

    return res;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const addNewList = async (namee, boardsId) => {
  try {
    const res = await axios.post(
      `http://localhost:3000/boards/${boardsId}/lists`,
      namee
    );

    return res;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const deleteTheList = async (listId) => {
  try {
    const res = await axios.delete(
      `http://localhost:3000/boards/lists/${listId}`
    );

    return res;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

//========================================================================

export const fetchCardsData = async (listId) => {
  try {
    const res = await axios.get(`http://localhost:3000/boards/${listId}/cards`);
    return res;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const addCardData = async (namee, listId) => {
  try {
    const res = await axios.post(
      `http://localhost:3000/boards/${listId}/cards`,
      namee
    );

    return res;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const deleteCardData = async (Id) => {
  try {
    const res = await axios.delete(`http://localhost:3000/boards/cards/${Id}`);
    return res;
  } catch (error) {
    console.log(error);
    throw error;
  }
};
