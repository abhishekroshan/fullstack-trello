import {
  Button,
  Card,
  Grid,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Input,
  FormControl,
  Box,
} from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import { useNavigate } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { useToast } from "@chakra-ui/react";
import { fetchBoardsData, addNewBoard, deleteBoard } from "../Api";
const AllBoards = () => {
  const [adding, setAdding] = useState(false);
  const [newBoardName, setNewBoardName] = useState("");

  const navigate = useNavigate();
  const toast = useToast();

  //==============================================================

  const [boards, setBoards] = useState([]);
  const [errors, setErrors] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchBoard = async () => {
      try {
        const data = await fetchBoardsData();

        setBoards(data.data);
        setLoading(false);
      } catch (error) {
        console.log(error);
        setErrors(true);
      }
    };

    fetchBoard();
  }, []);

  //=============================================================

  const handleDelete = async (idd) => {
    try {
      await deleteBoard(idd);
      const remaining = boards.filter((item) => {
        return item._id !== idd;
      });

      setBoards(remaining);

      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
      console.log(error);
    }
  };

  //==============================================================

  const handleOpen = () => setAdding(true);

  const handleClose = () => {
    setAdding(false);
  };

  const handleSubmit = async () => {
    try {
      const res = await addNewBoard({ name: newBoardName });
      setBoards((prevBoards) => [...prevBoards, res.data]);
      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
    setNewBoardName("");
    handleClose();
  };

  //   console.log(allboard);

  //================================================================

  return (
    <>
      {errors ? (
        <Box display="flex" justifyContent="center" fontSize="3rem">
          Error
        </Box>
      ) : loading ? (
        <Box display="flex" justifyContent="center" fontSize="3rem">
          Loading...
        </Box>
      ) : (
        <Grid templateColumns="repeat(6, 1fr)" h="100vh">
          {boards &&
            boards.map((item) => (
              <Card
                key={item._id}
                mx="2rem"
                my="1rem"
                h="5rem"
                w="15rem"
                p="10px"
                bg="rgb(128, 130, 245)"
                color="white"
                fontWeight="800"
                onClick={() => navigate(`boards/${item._id}`)}
              >
                {item.name}

                <DeleteIcon
                  onClick={(e) => {
                    e.stopPropagation();
                    handleDelete(item._id);
                  }}
                />
              </Card>
            ))}
          <Button onClick={handleOpen} mx="2.3rem" my="1rem">
            Add Board
          </Button>
          <Modal isOpen={adding} onClose={handleClose}>
            <ModalContent>
              <ModalHeader>Add New Board</ModalHeader>
              <ModalCloseButton />
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleSubmit();
                }}
              >
                <ModalBody>
                  <FormControl>
                    <Input
                      type="text"
                      value={newBoardName}
                      onChange={(e) => setNewBoardName(e.target.value)}
                      placeholder="Enter Board Name"
                    />
                  </FormControl>
                </ModalBody>
                <ModalFooter gap="10px">
                  <Button color="blue" type="submit">
                    Add
                  </Button>
                  <Button onClick={handleClose}>Cancel</Button>
                </ModalFooter>
              </form>
            </ModalContent>
          </Modal>
        </Grid>
      )}
    </>
  );
};

export default AllBoards;
