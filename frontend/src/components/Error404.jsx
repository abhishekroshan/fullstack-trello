import { Flex } from "@chakra-ui/react";
import React from "react";

const Error404 = () => {
  return (
    <>
      <Flex justifyContent="center" fontSize="3rem" fontWeight="600">
        Error 404!
      </Flex>
    </>
  );
};

export default Error404;
