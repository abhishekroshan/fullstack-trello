import React from "react";
import App from "./App";
import { Routes, Route } from "react-router-dom";
// import Header from "./components/Header";
import Error404 from "./components/Error404";
import Lists from "./components/Lists";
const RoutesFile = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/boards/:id" element={<Lists />} />
        <Route path="*" element={<Error404 />} />
      </Routes>
    </>
  );
};

export default RoutesFile;
